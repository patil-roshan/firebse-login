import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "./src/screens/appScreens/Home/Home";
import OnBoarding from "./src/screens/authentication/OnBoarding/OnBoarding";
import SignIn from "./src/screens/authentication/SignIn/SignIn";
import SignUp from "./src/screens/authentication/SignUp/SignUp";
import auth from '@react-native-firebase/auth';

const App = () => {

  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  const onAuthStateChanged = (user) => {
    setUser(user);
    if (initializing) setInitializing(false);
  }


  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
        headerShown: false,
        animation: 'ios'
      }}>
        {
          user ?
            <Stack.Screen name="Home" component={Home} />
            :
            <>
              <Stack.Screen name="Onboarding" component={OnBoarding} />
              <Stack.Screen name="SignIn" component={SignIn} />
              <Stack.Screen name="SignUp" component={SignUp} />
            </>
        }
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
