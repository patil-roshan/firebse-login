import auth from "@react-native-firebase/auth";
import { GoogleSignin } from '@react-native-google-signin/google-signin';

GoogleSignin.configure({
    webClientId: '801635879034-vjc83ang103fc1acl919k5vb9o40h63l.apps.googleusercontent.com'
});


export const CreateAccountWithEmailAndPassowd = ({ email, password }) => {
    return auth().createUserWithEmailAndPassword(email, password)
}
export const signInWithEmailAndPassword = ({ email, password }) => {
    return auth().signInWithEmailAndPassword(email, password)
}
export const signInWithGoogle = async () => {
    await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
};

export const signInAnonymously = () => {
    return auth().signInAnonymously()
}

export const signOutUSer = () => {
    return auth().signOut()
}