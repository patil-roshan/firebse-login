export default COLORS = {
    white: "#FFFFFF",
    black: "#000000",
    lightText: "#00000090",
    onBoardCardBg: "#EBC8FA",
    bgLineGradeOne: '#FFFEFE',
    bgLineGradeTwo: '#F6F5FC',
    bgLineGradeThree: '#EEEBF9',
    bgLineGradeFour: '#E3E6F6',
    bgLineGradeFive: '#DCE2EC',
    bgLineGradeSix: '#E5DEE4',
    transparent: 'transparent',

    warning: '#F88070',
    accent: '#FF6060',
}