import { ScrollView, StatusBar, StyleSheet, Text, TextInput, ToastAndroid, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import LinearGradient from 'react-native-linear-gradient'
import COLORS from '../../../constants/COLORS'
import Ionic from 'react-native-vector-icons/Ionicons';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { signInAnonymously, signInWithEmailAndPassword, signInWithGoogle } from '../../../utilities/Utilities';

const SignIn = ({ navigation }) => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [hidePassword, setHidePassword] = useState(false)

    const [showError, setShowError] = useState(false)
    const [errors, setErrors] = useState({})
    const getError = (email, password, confirmPassword) => {
        const errors = {};
        if (!email) {
            errors.email = "Please enter email."
        } else if (!email.includes("@") || !email.includes(".")) {
            errors.email = "Please enter valid email."
        }

        if (!password) {
            errors.password = "Please enter password."
        } else if (password.length < 8) {
            errors.password = "Please enter pasword of minimum 8 characters."
        }
        return errors;
    }

    const handleRegister = () => {
        const errors = getError(email, password);
        if (Object.keys(errors).length > 0) {
            setShowError(true)
            setErrors(showError && errors)
            console.log(errors);
        }
        else {
            setErrors({})
            setShowError(false)
            handleSignIn({ email: email, password: password })
        }
    };

    const handleSignIn = ({ email, password }) => {
        // console.log(email, password);
        signInWithEmailAndPassword({ email, password })
            .then(() => ToastAndroid.show("Logged in successfully", ToastAndroid.SHORT))
            .catch(error => {
                if (error.code === "auth/invalid-credential") {
                    setErrors({ email: "invalid credential", password: "invalid credential" })
                    // console.log(errors);
                    return;
                }
                // console.log(error);
            })
    }

    const LoginWithIcon = ({ iconName, onPress, butonTitle }) => {
        return (
            <TouchableOpacity onPress={onPress} style={{
                width: '40%',
                paddingVertical: 12,
                paddingHorizontal: COLORS.transparent,
                borderWidth: 2,
                borderColor: COLORS.white,
                borderRadius: 10,
                alignItems: "center",
                justifyContent: "center",
            }}>
                <Ionic
                    name={iconName} style={{
                        fontSize: 26,
                        color: COLORS.black,
                        marginBottom: 4,
                    }} />
                <Text style={{
                    fontSize: 16,
                    color: COLORS.black,
                    opacity: 0.4
                }}>
                    {butonTitle}
                </Text>
            </TouchableOpacity>
        )
    }

    return (
        <View>
            <StatusBar barStyle='dark-content' backgroundColor={COLORS.bgLineGradeOne} />
            <LinearGradient
                colors={[
                    COLORS.bgLineGradeOne,
                    COLORS.bgLineGradeTwo,
                    COLORS.bgLineGradeThree,
                    COLORS.bgLineGradeFour,
                    COLORS.bgLineGradeFive,
                    COLORS.bgLineGradeSix
                ]}
                style={{ width: "100%", height: "100%" }}>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => navigation.goBack()}
                    style={{
                        backgroundColor: COLORS.white,
                        width: 40,
                        aspectRatio: 1 / 1,
                        alignItems: "center",
                        borderRadius: 100,
                        elevation: 4,
                        position: "absolute",
                        top: 20,
                        left: 20,
                        zIndex: 100,
                    }}>
                    <Ionic
                        name="chevron-back"
                        style={{
                            fontSize: 20,
                            color: COLORS.black,
                            flex: 1,
                            paddingVertical: 10,
                        }}
                    />
                </TouchableOpacity>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{
                        padding: 30,
                    }}>
                    <Text
                        style={{
                            textAlign: 'center',
                            marginTop: 100,
                            marginBottom: 20,
                            // marginVertical: 20,
                            fontSize: 30,
                            color: COLORS.black,
                            letterSpacing: 2,
                            fontWeight: 500,
                        }}>
                        Hello, again!
                    </Text>
                    <Text
                        style={{
                            textAlign: 'center',
                            fontSize: 20,
                            marginBottom: 50,
                            color: COLORS.black,
                            fontWeight: 400,
                            opacity: 0.4
                        }}>
                        Welcone back, you've been missed
                    </Text>

                    <View style={{
                        width: '100%',
                    }}>
                        <View style={{
                            width: '100%',
                            marginBottom: 20
                        }}>
                            <TextInput
                                placeholder='Email'
                                placeholderTextColor={COLORS.lightText}
                                keyboardType='email-address'
                                value={email}
                                onChangeText={(e) => setEmail(e)}
                                style={{
                                    paddingVertical: 10,
                                    paddingHorizontal: 20,
                                    fontSize: 14,
                                    color: COLORS.black,
                                    borderRadius: 10,
                                    backgroundColor: COLORS.white
                                }}
                            />
                            {errors.email && <Text style={{ color: COLORS.warning, margin: 4 }}>{errors.email}</Text>}
                        </View>
                        <View style={{
                            width: '100%',
                            marginBottom: 20
                        }}>
                            <View style={{
                                borderRadius: 10,
                                backgroundColor: COLORS.white,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: "space-between"
                            }}>
                                <TextInput
                                    placeholder='password'
                                    placeholderTextColor={COLORS.lightText}
                                    keyboardType='default'
                                    maxLength={8}
                                    secureTextEntry={hidePassword ? false : true} value={password}
                                    onChangeText={(e) => setPassword(e)}
                                    style={{
                                        paddingVertical: 10,
                                        paddingHorizontal: 20,
                                        fontSize: 14,
                                        color: COLORS.black,
                                        borderRadius: 10,
                                        backgroundColor: COLORS.white
                                    }}
                                />
                                {
                                    password.length > 0 &&
                                    <TouchableOpacity
                                        activeOpacity={0.6}
                                        style={{ paddingHorizontal: 10, paddingRight: 20 }}
                                        onPress={() => setHidePassword(!hidePassword)}
                                    >
                                        <Ionic name={hidePassword ? "eye-sharp" : "eye-off-sharp"} style={{
                                            fontSize: 20,
                                            color: COLORS.black,
                                        }} />
                                    </TouchableOpacity>
                                }
                            </View>
                            {errors.password && <Text style={{ color: COLORS.warning, padding: 10 }}>{errors.password}</Text>}

                        </View>

                        <TouchableOpacity
                            onPress={() => handleRegister()}
                            activeOpacity={0.8}
                            style={{
                                width: "100%",
                                paddingVertical: 14,
                                paddingHorizontal: 20,
                                alignItems: "center",
                                justifyContent: "center",
                                backgroundColor: COLORS.accent,
                                borderRadius: 10,
                                elevation: 8,
                                shadowColor: COLORS.accent,
                            }}>
                            <Text style={{
                                color: COLORS.white,
                                fontSize: 16
                            }}
                            >
                                Sign in
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: "center",
                            justifyContent: "center",
                            marginVertical: 30,
                        }}
                    >
                        <LinearGradient
                            start={{ x: 1, y: 0 }}
                            end={{ x: 0.5, y: 1.0 }}
                            colors={['#00000090', '#00000090', '#ffffff00']}
                            style={{
                                flex: 1,
                                paddingVertical: 1.4,
                                borderRadius: 100,

                            }}>
                        </LinearGradient>
                        <Text style={{
                            fontSize: 20,
                            color: COLORS.black,
                            opacity: 0.4,
                            marginHorizontal: 5,
                        }}>
                            or Continue
                        </Text>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            colors={['#00000090', '#00000090', '#ffffff00']}
                            style={{
                                flex: 1,
                                paddingVertical: 1.4,
                                borderRadius: 100,
                            }}></LinearGradient>
                    </View>

                    <View style={{
                        flexDirection: "row",
                        justifyContent: "space-around",
                        alignItems: "center",
                        marginTop: 10,
                        marginBottom: 40
                    }}>
                        < LoginWithIcon
                            iconName="logo-google"
                            onPress={() => signInWithGoogle().then(() =>
                                ToastAndroid.show("Signed in with google", ToastAndroid.SHORT))
                                .catch(err => console.log('err:', err))
                            }
                            butonTitle="Google"
                        />
                        < LoginWithIcon
                            iconName="person"
                            onPress={() => signInAnonymously().then(() =>
                                ToastAndroid.show("Signed in Anonymously", ToastAndroid.SHORT))
                                .catch(err => console.log('err:', err))
                            } butonTitle="Anonymous"
                        />
                    </View>


                    <TouchableOpacity
                        onPress={() => navigation.navigate("SignUp")}
                        style={{
                            width: "100%",
                            alignItems: 'center'
                        }}
                    >
                        <Text style={{
                            fontSize: 16,
                            fontWeight: 400,
                            color: Colors.black
                        }}>Don't have an account?{" "}
                            <Text style={{
                                color: COLORS.accent,
                                textDecorationLine: 'underline'
                            }}>Sign Up</Text>
                        </Text>
                    </TouchableOpacity>
                </ScrollView>
            </LinearGradient>
        </View >
    )
}

export default SignIn

const styles = StyleSheet.create({})