import React from 'react'
import { Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import COLORS from '../../../constants/COLORS'
import LinearGradient from 'react-native-linear-gradient';

const OnBoarding = ({ navigation }) => {
    return (
        <View>
            <StatusBar
                barStyle='dark-content'
                backgroundColor={COLORS.bgLineGradeOne}
            />
            <LinearGradient
                colors={[
                    COLORS.bgLineGradeOne,
                    COLORS.bgLineGradeTwo,
                    COLORS.bgLineGradeThree,
                    COLORS.bgLineGradeFour,
                    COLORS.bgLineGradeFive,
                    COLORS.bgLineGradeSix
                ]}
                style={{ width: "100%", height: "100%" }}>
                <View style={{ width: "100%", height: "50%", padding: 16 }}>
                    <View
                        style={{
                            width: "100%",
                            height: "100%",
                            backgroundColor: COLORS.onBoardCardBg,
                            borderRadius: 20,
                            justifyContent: "center",
                            alignItems: "center",
                        }}>
                        <Image
                            source={require("../../../images/authentication/notesBg.png")}
                            style={{ height: "70%", aspectRatio: 1 / 1 }}
                        />
                    </View>
                    <View
                        style={{
                            width: "100%",
                            justifyContent: "center",
                            alignItems: "center",
                            marginTop: 40,
                            marginBottom: 20,
                        }} >
                        <Text style={{
                            fontSize: 24,
                            color: COLORS.black,
                            fontWeight: 800,
                            letterSpacing: 1,
                        }}>
                            Simplify your notes
                        </Text>
                        <Text style={{
                            fontSize: 24,
                            color: COLORS.black,
                            fontWeight: 800,
                            letterSpacing: 1,
                        }}>
                            Boost your Productivity
                        </Text>
                    </View>

                    <View style={{
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: 40,
                        marginBottom: 20,

                    }} >

                        <View style={{ width: "60%", flexDirection: "row", }}>
                            <TouchableOpacity
                                onPressOut={() => navigation.navigate("SignUp")}
                                activeOpacity={0.8}
                                style={{
                                    width: "50%",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    paddingVertical: 16,
                                    borderColor: COLORS.transparent,
                                    backgroundColor: COLORS.white,
                                    borderTopLeftRadius: 10,
                                    borderBottomLeftRadius: 10,
                                }}>
                                <Text style={{
                                    fontSize: 20,
                                    fontWeight: 600,
                                    color: COLORS.black
                                }}>Register</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPressOut={() => navigation.navigate("SignIn")}
                                activeOpacity={0.8}
                                style={{
                                    width: "50%",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    paddingVertical: 16,
                                    backgroundColor: COLORS.transparent,
                                    borderWidth: 2,
                                    borderColor: COLORS.white,
                                    borderTopRightRadius: 10,
                                    borderBottomRightRadius: 10,
                                }}>

                                <Text style={{
                                    fontSize: 20,
                                    fontWeight: 600,
                                    color: COLORS.black
                                }}>Login</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>



            </LinearGradient>

        </View>
    )
}

export default OnBoarding

const styles = StyleSheet.create({})