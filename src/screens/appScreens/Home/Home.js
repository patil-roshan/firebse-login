import { StyleSheet, Text, View, TouchableOpacity, StatusBar } from 'react-native'
import React from 'react'
import COLORS from '../../../constants/COLORS'
import LinearGradient from 'react-native-linear-gradient'
import { signOutUSer } from '../../../utilities/Utilities'
import auth from "@react-native-firebase/auth";


const Home = () => {
    const handleSignOut = () => {
        try {
            signOutUSer();
            console.log("Signed out successfully");
        } catch (error) {
            console.log(error);
        }
    }
    const user = auth().currentUser;
    console.log(user);

    return (
        <View>
            <StatusBar
                backgroundColor={COLORS.bgLineGradeOne}
                barStyle="dark-content"
            />

            <LinearGradient
                colors={[
                    COLORS.bgLineGradeOne,
                    COLORS.bgLineGradeTwo,
                    COLORS.bgLineGradeThree,
                    COLORS.bgLineGradeFour,
                    COLORS.bgLineGradeFive,
                    COLORS.bgLineGradeSix
                ]}
                style={{ width: "100%", height: "100%", alignItems: "center", justifyContent: "center" }}>
                <Text style={{
                    color: COLORS.black,
                    fontSize: 20,
                }}>
                    Welcome
                </Text>
                <Text style={{
                    color: COLORS.black,
                    fontSize: 40,
                    fontWeight: "700",
                    letterSpacing: 2,
                    marginTop: 20,
                    marginBottom: 40,
                }}>
                    {user.displayName ? user.displayName : "Anonymous User"}
                </Text>

                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => handleSignOut()}
                    style={{
                        marginTop: 20,
                        backgroundColor: COLORS.warning,
                        paddingVertical: 8,
                        paddingHorizontal: 20,
                        borderRadius: 10,
                    }}>
                    <Text>SignOut</Text>
                </TouchableOpacity>
            </LinearGradient>
            {/* <Text>Home</Text> */}
        </View >
    )
}

export default Home

const styles = StyleSheet.create({})